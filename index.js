// console.log("Hello World!")


function addition (num1, num2){
	let sum = num1 + num2;
	console.log("Displayed sum of " +num1+ " and " +num2);
	console.log(sum);
}
addition(5 , 15);

function subtraction (num3, num4){
	let difference = num3 - num4;
	console.log("Displayed difference of " +num3+ " and " +num4);
	console.log(difference);
}
subtraction(20 , 5);

function multiplication (num5, num6){
	let product = num5 * num6;
	console.log("The product of " +num5+ " and " +num6+ ":");
	console.log(product);
}
multiplication(50 , 10);

function returnProduct(num5, num6){
	return num5 , num6;
}

function division (num7, num8){
	let quotient = num7 / num8;
	console.log("The quotient of " +num7+ " and " +num8+ ":");
	console.log(quotient);
}
division(50 , 10);

let product = returnProduct(multiplication);

function returnQuotient(num7, num8){
	return num7 , num8;
}
let quotient = returnQuotient(division);

function circleArea(radius){
	let area = (radius * radius)*3.1416;
	console.log("The result of getting the area of a circle with " +radius+ " radius:");
	console.log(area);
}
circleArea(15);

function returnCircleArea(radius){
	return radius;
}


function ave (num9, num10, num11, num12){
	let average = (num9 + num10 + num11 + num12) / 4;
	console.log("The average of " +num9 + " " +num10+ " " + num11 + " and " +num12+ " : ");
	return average;
}
let averageVar = ave(20, 40, 60, 80);
console.log(averageVar);

function passing (num13, num14){
	let percentage = (num13 / num14) * 100;
	let isPassed = percentage >= 75;
	return isPassed;
}
let isPassingScore = passing (38, 50);
console.log("Is 37/50 a passing score?")
console.log(isPassingScore);
